# ProxyByrd

## Introduction

This is a package inspired by [nano-gateway](https://github.com/sinedied/nano-gateway) but that has been rewritten in typescript with extra features. ProxyByrd lets you serve your websites with HTML and PHP (php coming soon), as well as proxy to other web servers.

## Usage

### Installation

```bash
# NPM
npm i -g @flowtr/proxybyrd
# Yarn
yarn global add @flowtr/proxybyrd
# PNPM
pnpm i -g @flowtr/proxybyrd
```

### Run

You can run ProxyByrd with its CLI command, optionally specifying the config file path. It currently only supports yaml/yml but I plan on adding json5 and toml support.

```bash
proxybyrd -c config.yml
```

### Example Configuration

```yml
http:
    port: 8080
auth: false
apiKey: "123456"
services:
    index:
        path: "/"
        to:
            type: "render"
            path: "test.html"
```

### More Documentation

[More documentation is coming soon.](https://docs.toes.tech)

## Issues

[Let me know if you have any issues here.](https://gitlab.com/creepinson/proxy-server/-/issues)
