import express, {
    Request,
    Response,
    Application,
    NextFunction,
    RequestHandler,
} from "express";
import vhost from "vhost-ts";
import fs from "fs";
import path from "upath";
import https from "https";
import http from "http";
import { createProxyMiddleware } from "http-proxy-middleware";
import rewrite from "express-urlrewrite";
import cors from "cors";
import { Config, logger, Service } from "./config";
import { WriteConfig } from "@theoparis/config";
import { ImprovedError } from "@toes/core";

export const hasValidScheme = (s: string) => /^https?:\/\//.test(s);

export interface AppResult {
    app: Application;
    auth: RequestHandler;
    handleService: ([name, service]) => void;
}

export const createApp = (config: WriteConfig<Config>): AppResult => {
    const app = express();
    app.use(cors());

    const onProxyError = (err, req, res) => {
        const code = err.code;

        if (res.writeHead && !res.headersSent)
            if (/HPE_INVALID/.test(code)) res.writeHead(502);
            else
                switch (code) {
                    case "ECONNRESET":
                    case "ENOTFOUND":
                    case "ECONNREFUSED":
                        res.writeHead(504);
                        break;
                    default:
                        res.writeHead(500);
                }

        res.end(`Error occured while trying to proxy to: ${req.url}`);
    };

    const defaultHandler = (_req, _res, next: NextFunction) => next();

    const createServeHandler = (fPath: string): RequestHandler => {
        const isValid = (p: string) =>
            fs.existsSync(p) && fs.lstatSync(p).isFile();
        return (req, res, next) => {
            const urlPath = req.originalUrl.endsWith("/")
                ? req.originalUrl.slice(0, -1)
                : req.originalUrl;
            const file = path.join(fPath, urlPath);
            if (
                req.originalUrl === "/" &&
                isValid(path.join(fPath, "index.html"))
            )
                return res.sendFile(path.join(fPath, "index.html"));
            else if (isValid(file)) return res.sendFile(file);
            else {
                const fIndexPath = path.join(
                    fPath,
                    path.dirname(urlPath),
                    "index.html"
                );
                const fHtmlPath = path.join(
                    fPath,
                    path.dirname(urlPath),
                    `${path.basename(urlPath)}.html`
                );
                if (isValid(fHtmlPath)) return res.sendFile(fHtmlPath);
                else if (isValid(fIndexPath)) return res.sendFile(fIndexPath);
                else next();
            }
        };
    };

    const handleService = ([name, service]: [string, Service]) => {
        logger.debug(`service: ${name} at ${service.path}`);

        const routeConfig = [
            service.auth ||
            (service.auth === undefined && config.toObject().auth)
                ? auth
                : defaultHandler,
            service.rewrite
                ? rewrite(
                      service.rewrite.from || service.path,
                      service.rewrite.to
                  )
                : defaultHandler,
            service.to.type === "proxy"
                ? createProxyMiddleware({
                      target: service.to.url,
                      onError: onProxyError,
                  })
                : service.to.type === "send"
                ? (_req: Request, res: Response) => {
                      const to = service.to as Record<string, unknown>;
                      return res
                          .status(to.status as number)
                          .header(
                              "content-type",
                              typeof to.data === "string"
                                  ? "text/plain"
                                  : "application/json"
                          )
                          .send(
                              typeof to.data === "string"
                                  ? to.data
                                  : JSON.stringify(to.data)
                          );
                  }
                : service.to.type === "serve"
                ? createServeHandler(path.join(process.cwd(), service.to.path))
                : (_req: Request, res: Response) =>
                      res.sendFile(
                          path.join(
                              process.cwd(),
                              (service.to as Record<string, unknown>)
                                  .path as string
                          )
                      ),
        ];
        if (service.domain)
            (typeof service.domain === "string"
                ? [service.domain]
                : service.domain
            ).forEach((d) => app.use(vhost(d, (req, res, next) => {})));
        else app.all(service.path, ...routeConfig);
    };
    const auth = (req: Request, res: Response, next: NextFunction) => {
        const apiKey: string = (req.headers.authorization ||
            req.query.apiKey) as string;
        if (apiKey && apiKey === config.toObject().apiKey) return next();

        const error = new ImprovedError({ message: "Unauthorized", code: 401 });
        res.status(401);
        return next(error);
    };
    Object.entries(config.toObject().services).forEach(handleService);
    return { app, auth, handleService };
};

export interface ServerResult extends AppResult {
    httpServer: http.Server;
    httpsServer: https.Server;
}

export const createServer = (config: WriteConfig<Config>): ServerResult => {
    const { app, ...res } = createApp(config);

    let httpServer: http.Server;
    let httpsServer: https.Server;
    function showAddress(type: string) {
        const bound = {
            address:
                type === "http"
                    ? config.toObject().http.host
                    : config.toObject().https.host,
            port:
                type === "http"
                    ? config.toObject().http.port
                    : config.toObject().https.port,
        };
        return () =>
            logger.info(
                `Listening on ${type}://${bound.address}:${bound.port}`
            );
    }

    if (config.toObject().http || !config.toObject().https)
        httpServer = app.listen(
            config.toObject().http.port || 8080,
            config.toObject().http.host,
            showAddress("http")
        );

    if (
        config.toObject().https &&
        config.toObject().https.key &&
        config.toObject().https.cert
    ) {
        const key = fs.readFileSync(
            path.join(process.cwd(), config.toObject().https.key, "utf8")
        );
        const cert = fs.readFileSync(
            path.join(process.cwd(), config.toObject().https.cert),
            "utf8"
        );
        const ca = config.toObject().https.ca
            ? fs.readFileSync(
                  path.join(process.cwd(), config.toObject().https.ca),
                  "utf8"
              )
            : undefined;
        httpsServer = https
            .createServer({ key, cert, ca }, app)
            .listen(config.toObject().https.port || 8443, showAddress("http"));
    }
    return { httpServer, httpsServer, app, ...res };
};
