import * as z from "zod";
import { nanoid } from "nanoid";

export const unixPathSchema = z.string(); /* .regex(/[^\\/]+\.[^\\/]+$/); */

export const serviceSchema = z.object({
    domain: z.union([z.string(), z.array(z.string())]).optional(),
    path: z.string().default("/"),
    rewrite: z
        .object({
            from: z.string().default("/"),
            to: z.string(),
        })
        .optional(),
    auth: z.boolean().optional(),
    to: z.union([
        z.object({ type: z.literal("proxy"), url: z.string() }),
        z.object({
            type: z.union([z.literal("render"), z.literal("serve")]),
            path: unixPathSchema,
        }),
        z.object({
            type: z.literal("send"),
            data: z.unknown(),
            status: z.number().default(200),
        }),
    ]),
});

export const configSchema = z.object({
    http: z.object({
        host: z.string().default("0.0.0.0"),
        port: z.number().default(80),
    }),
    https: z
        .object({
            cert: unixPathSchema,
            key: unixPathSchema,
            ca: unixPathSchema,
            host: z.string().default("0.0.0.0"),
            port: z.number().default(443),
        })
        .optional(),
    auth: z.boolean(),
    apiKey: z.string().min(5).default(nanoid(20)),
    services: z.record(serviceSchema).default({}),
});

export type Config = z.infer<typeof configSchema>;
export type Service = z.infer<typeof serviceSchema>;
