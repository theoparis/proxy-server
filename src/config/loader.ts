import { Config, configSchema } from "./config";
import { readFile } from "@theoparis/config";
import path from "upath";
import { logger } from "./logger";
import fs from "fs";
export const config = () => {
    const p = path.join(
        process.cwd(),
        process.env.PROXY_BYRD_CONFIG || "config.yml"
    );
    if (!fs.existsSync(p)) {
        logger.error(`Config file doesn't exist at: ${p}`);
        process.exit(1);
    }
    return readFile<Config>(p, { type: "yaml", schema: configSchema }).validate(
        true,
        (err) => {
            logger.error(err);
            process.exit(1);
        }
    );
};
