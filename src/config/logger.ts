import { PrefixLogger } from "@toes/logger";

export const logger = new PrefixLogger({
    prefix: "ProxyByrd",
    separator: " > ",
    color: true,
});
